#include "SerialCom.h"

#include <iostream>

SerialCom::SerialCom(unsigned int portNumber, Logger* logger)
   : logger_(logger)
{
   std::string portName = "\\\\.\\COM";
   portName += std::to_string(portNumber);

   handle_ = CreateFileA(static_cast<LPCSTR>(portName.c_str()), GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);

   if (handle_ == INVALID_HANDLE_VALUE)
   {
      logger_->Log("Error creating handle.", LogMessageType::ERROR_MSG);
   }
   else
   {
      DCB serialParams = DCB();

      if (!GetCommState(handle_, &serialParams))
      {
         logger_->Log("Error getting serial config.", LogMessageType::ERROR_MSG);
      }
      else
      {
         serialParams.BaudRate = CBR_9600;
         serialParams.ByteSize = 8;
         serialParams.StopBits = ONESTOPBIT;
         serialParams.Parity = NOPARITY;
         serialParams.fDtrControl = DTR_CONTROL_ENABLE;

         if (!SetCommState(handle_, &serialParams))
         {
            logger_->Log("Error setting serial config", LogMessageType::ERROR_MSG);
         }
         else
         {
            connected_ = true;
            PurgeComm(handle_, PURGE_RXCLEAR);
         }
      }
   }
}

SerialCom::~SerialCom()
{
   if (connected_)
   {
      connected_ = false;
      CloseHandle(handle_);
   }
}

unsigned int SerialCom::ReadCom(char* buffer, unsigned int bufferSize)
{
   ClearCommError(handle_, &errors_, &status_);

   unsigned int bytesToRead = 0;

   if (status_.cbInQue > bufferSize)
      bytesToRead = bufferSize;
   else
      bytesToRead = status_.cbInQue;

   DWORD bytesRead = 0;
   ReadFile(handle_, buffer, bytesToRead, &bytesRead, NULL);
   return bytesRead;
}

unsigned int SerialCom::WriteCom(const char* buffer, unsigned int bufferSize)
{
   DWORD bytesSent = 0;

   if (!WriteFile(handle_, (void*)buffer, bufferSize, &bytesSent, 0))
   {
      ClearCommError(handle_, &errors_, &status_);
   }

   return bytesSent;
}

bool SerialCom::IsConnected()
{
   return connected_;
}