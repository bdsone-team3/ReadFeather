#pragma once

#include <string>
#include <fstream>

enum LogMessageType : unsigned int
{
  INFO,
  WARNING,
  ERROR_MSG,
  DEBUG
};

class Logger
{
public:
  Logger(std::string logfile = "FeatherMonitor.log");
  ~Logger();
  void Log(std::string message, LogMessageType msgType = LogMessageType::INFO);
protected:
  std::ofstream logFile;
};