#pragma once

#include <Windows.h>

#include "Logger.h"

#include <string>

class SerialCom
{
public:
  SerialCom(unsigned int portNumber, Logger* logger);
  ~SerialCom();
  unsigned int ReadCom(char* buffer, unsigned int bufferSize);
  unsigned int WriteCom(const char* buffer, unsigned int bufferSize);
  bool IsConnected();
private:
  HANDLE handle_;
  bool connected_;
  COMSTAT status_;
  DWORD errors_;
  Logger* logger_;
};