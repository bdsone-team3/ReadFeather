#include "FeatherMonitor.h"

#include <sstream>
#include <string>

#define RECEIVE_BUFFER_SIZE 0x10000

FeatherMonitor::FeatherMonitor()
   : serialFeather_(nullptr)
   , serialRtk_(nullptr)
   , logger_(nullptr)
{

}

void FeatherMonitor::Init(unsigned int featherReadPort, unsigned int rtkWritePort, Logger* logger)
{
   serialFeather_ = new SerialCom(featherReadPort, logger);
   serialRtk_ = new SerialCom(rtkWritePort, logger);
   logger_ = logger;
}

void FeatherMonitor::Run()
{
   if (serialFeather_->IsConnected())
   {
      char rxBuffer[RECEIVE_BUFFER_SIZE];

      // Loop forever. :)
      // TODO: add some logic to break the loop and cleanly exit the program.
      while (true)
      {
         // Read all the data queued up
         unsigned int count = serialFeather_->ReadCom(rxBuffer, RECEIVE_BUFFER_SIZE);
         if (count != 0)
         {
            std::istringstream ss;
            ss.str(rxBuffer);
            for (std::string line; std::getline(ss, line, '|'); )
            {
               if (line.rfind((char)0xB5, 0) == 0)
               {
                  serialRtk_->WriteCom(line.data(), line.length());
               }
               else
               {
                  IMU_DataType data = IMU_DataType();
                  if (sizeof(line.data()) == sizeof(data))
                  {
                     memcpy(&data, line.data(), sizeof(data));
                     char buffer[100];
                     sprintf_s(buffer, "X: %f Y: %f Z: %f\r\n", data.eulerX, data.eulerY, data.eulerZ);
                     logger_->Log(buffer, LogMessageType::INFO);
                  }
               }
            }
         }

         // Sleep and wait for more data.
         // Prevents the loop from spinning too fast if no data is coming in.
         Sleep(100);
      }
   }
   else
   {
      logger_->Log("Unable to Read Feather, not connected.", LogMessageType::ERROR_MSG);
   }
}