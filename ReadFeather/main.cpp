#include "FeatherMonitor.h"
#include "Logger.h"

#include <iostream>

static auto mylog = new Logger("ReadFeather.log");
static auto monitor = FeatherMonitor();

int main(int argc, char** argv)
{
  if (argc > 2)
  {
    unsigned int featherPort = std::stoi(argv[1]);
    unsigned int rtkPort = std::stoi(argv[2]);
    monitor.Init(featherPort, rtkPort, mylog);

    monitor.Run();
  }
  else
  {
    mylog->Log("Expecting COM Number input argument", LogMessageType::ERROR_MSG);
  }

  return 0;
}

