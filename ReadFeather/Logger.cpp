#include "Logger.h"

#include <iostream>

Logger::Logger(std::string logfile)
  : logFile(logfile, std::ofstream::out)
{
  Log("Logging output to: " + logfile);
}

Logger::~Logger()
{
  logFile.close();
}

void Logger::Log(std::string message, LogMessageType msgType)
{
  std::string header = "";
  switch (msgType)
  {
  case INFO:
    header = "INFO:    ";
    break;
  case WARNING:
    header = "WARNING: ";
    break;
  case ERROR_MSG:
    header = "ERROR:   ";
    break;
  case DEBUG:
    header = "DEBUG:   ";
    break;
  }

  logFile   << header << message << std::endl;
  std::cout << header << message << std::endl;
}