#pragma once

#include "SerialCom.h"
#include "Logger.h"

struct IMU_DataType
{
  float accelX;
  float accelY;
  float accelZ;

  float magX;
  float magY;
  float magZ;

  float gyroX;
  float gyroY;
  float gyroZ;

  float eulerX;
  float eulerY;
  float eulerZ;

  float linAccelX;
  float linAccelY;
  float linAccelZ;

  float gravityX;
  float gravityY;
  float gravityZ;
};

class FeatherMonitor
{
public:
  FeatherMonitor();

  void Init(unsigned int featherReadPort, unsigned int rtkWritePort, Logger* logger);
  void Run();
protected:
  SerialCom* serialFeather_;
  SerialCom* serialRtk_;
  Logger* logger_;
};